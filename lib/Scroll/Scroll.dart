import 'package:flutter/material.dart';

class MyScroll extends StatefulWidget {
  const MyScroll({super.key});

  @override
  State<MyScroll> createState() => _MyScrollState();
}

class _MyScrollState extends State<MyScroll> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("MenuBar"),
        backgroundColor: Colors.blue,
        titleTextStyle: TextStyle(color: Colors.white, fontSize: 20),
      ),
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: 200,
                child: ListView.builder(
                  itemCount: 10,
                  scrollDirection: Axis.horizontal,
                  itemBuilder: (context, index) => Container(
                    height: 100,
                    width: 100,
                    margin: EdgeInsets.all(20),
                    child: Center(
                      child: Text("Cart $index"),
                    ),
                    color: Colors.green[700],
                  ),
                ),
              ),
              Flexible(
                child: ListView.builder(
                    itemCount: 15,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (context, index) => ListTile(
                    title: Text("List $index"),
                )),
              )

            ],
          ),
        ),
      ),
    );
  }
}
