import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shimmer/shimmer.dart';

class MyApiScroll extends StatefulWidget {
  const MyApiScroll({super.key});

  @override
  State<MyApiScroll> createState() => _MyApiScrollState();
}

class _MyApiScrollState extends State<MyApiScroll> {
  
  final String url = "https://fakestoreapi.com/products";
  Future<List<dynamic>> fetchData() async {
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return json.decode(response.body);
    }
    else{
      throw Exception("Failed to laod data");
    }
  }
  
  Widget buildShimmer(){
    return Shimmer.fromColors(
      baseColor: Colors.grey[300]!,
      highlightColor: Colors.grey[100]!,
      child: SizedBox(
        height: 200,
        child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemBuilder: (_, __) => Container(
            width: 200,
            margin: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              color: Colors.grey[700],
              borderRadius: BorderRadius.circular(10)
            ),
          ),
        ),
      ),
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Menu"),
        backgroundColor: Colors.blue,
        titleTextStyle: TextStyle(color: Colors.white, fontSize: 20),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            FutureBuilder<List<dynamic>>(
              future: fetchData(),
              builder: (context, snapshot){
                if(snapshot.connectionState == ConnectionState.waiting){
                  return buildShimmer();
                }
                else if(!snapshot.hasData || snapshot.data!.isEmpty){
                  return const Center(child: Text("Data not found"),);
                }
                else{
                  return SizedBox(
                    height: 220,
                    child: ListView.builder(
                      itemCount: snapshot.data!.length,
                      scrollDirection: Axis.horizontal,
                      itemBuilder: (context, index){
                        var item = snapshot.data![index];
                        return GestureDetector(
                          onTap: (){},
                          child: Container(
                            height: 200,
                            width: 200,
                            padding: EdgeInsets.all(10),
                            margin: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              color: Colors.grey[200],
                              borderRadius: BorderRadius.circular(10)
                            ),
                            child: Column(
                              children: [
                                Image.network(item['image'], width: 80,height: 80,fit: BoxFit.cover,),
                                SizedBox(height: 10,),
                                Text(item['title'],maxLines: 2,overflow: TextOverflow.ellipsis,),
                                ElevatedButton(onPressed: (){
                                  print("${item['id']}");
                                }, child: Text("Add to cart"))
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  );
                }
              }
            ),

          ],
        ),
      ),
    );
  }
}
