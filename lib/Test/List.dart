import 'dart:ffi';


import 'package:flutter/material.dart';
class MyList extends StatefulWidget {
  const MyList({super.key});

  @override
  State<MyList> createState() => _MyListState();
}

class _MyListState extends State<MyList> {

  List<Map<String, dynamic>> items = [
    {
      "id": 1,
      "url": "https://i5.walmartimages.com/asr/7497fd45-fece-4cc5-9c33-05ab195c985f.87a85446b3ee239f0495dc82a87788cf.jpeg",
      "name": "I phone 13",
      "price": 2000
    },
    {
      "id": 2,
      "url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRix_-6SVm42xTSbOG72AT6p4T27L1fJ_rDnw&s",
      "name": "I phone 11",
      "price": 1000
    },

    {
      "id": 3,
      "url": "https://adminapi.applegadgetsbd.com/storage/media/large/1533-48820.jpg",
      "name": "I phone 15 pro",
      "price": 1200
    },
    {
      "id": 4,
      "url": "https://assets.swappie.com/cdn-cgi/image/width=600,height=600,fit=contain,format=auto/swappie-iphone-12-pro-max-silver.png?v=9166c13e",
      "name": "I phone 12 pro max",
      "price": 700
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: Text("List"),
        titleTextStyle: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
      ),
      body: ListView.builder(
        itemCount: items.length,
        itemBuilder: (context, index){
          final item = items[index];
          return Card(
            child: ListTile(onTap: (){
                print("You were click on id ${item["id"]}");
            },
              leading: Image.network(item["url"]),
              title: Text(item["name"]!),
              subtitle: Text("Price: \$${item["price"]}"),
            ),
          );
        },
      ),
    );
  }
}
