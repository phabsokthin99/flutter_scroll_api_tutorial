import 'dart:convert';

import 'package:array/Api/TestApi.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
class MyAppList extends StatefulWidget {
  const MyAppList({super.key});

  @override
  State<MyAppList> createState() => _MyAppListState();
}

class _MyAppListState extends State<MyAppList> {

  String url = "https://fakestoreapi.com/products";
  Future<List<dynamic>> fetchData() async{
    final response = await http.get(Uri.parse(url));
    if(response.statusCode == 200){
      return json.decode(response.body);
    }
    else{
      throw Exception("Failed to load");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Menu Bar"),
        backgroundColor: Colors.blue,
        titleTextStyle: TextStyle(color: Colors.white, fontSize: 20),
      ),
      body: FutureBuilder(
        future: fetchData(),
        builder: (context, snapshot){
          if(snapshot.connectionState == ConnectionState.waiting){
            return Center(child:CircularProgressIndicator());

          }else if(!snapshot.hasData || snapshot.data!.isEmpty){
            return Center(child: Text("No data found"),);
          }
          else{
            return GridView.builder(
              itemCount: snapshot.data!.length,
              itemBuilder: (context, index){
                var item = snapshot.data![index];
                return GestureDetector(
                  onTap: () => {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => MyTest(),
                      settings: RouteSettings(
                        arguments: item
                      )
                    ))
                  },
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(padding: EdgeInsets.all(10)),
                          Image.network(item['image'], width: 70,height: 70,),
                          SizedBox(height: 10,),
                          Text(item['title'],maxLines: 2,),
                          SizedBox(height: 10,),
                          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(item['price'].toString()),
                              Icon(Icons.shopping_cart)
                            ],
                          ),
                          // Text(
                          //   item['description'],
                          //   overflow: TextOverflow.ellipsis,
                          //   style: TextStyle(
                          //     fontSize: 16.0,
                          //     color: Colors.black,
                          //   ),
                          // ),
                          
                        ],
                      ),
                    ),
                  ),
                );
              }, gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: 0.9,
              // crossAxisSpacing: 1,
              // mainAxisSpacing: 1
            ),
            );
          }
        },
      ),
    );
  }
}
