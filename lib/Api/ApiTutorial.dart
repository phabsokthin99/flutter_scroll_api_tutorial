
import 'dart:convert';

import 'package:array/Api/TestApi.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class MyApi extends StatefulWidget {
  const MyApi({super.key});

  @override
  State<MyApi> createState() => _MyApiState();
}

class _MyApiState extends State<MyApi> {

  String url = 'https://jsonplaceholder.typicode.com/users';

  Future<List<dynamic>> fetchData() async {
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to load data');
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue,
        titleTextStyle: TextStyle(color: Colors.white,fontSize: 30),
        title: Text("Api"),
      ),
      body: FutureBuilder(
        future: fetchData(),
        builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
        return Center(child: CircularProgressIndicator(color: Colors.red,strokeWidth: 3,));
        } else if (snapshot.hasError) {
        return Center(child: Text('Error: ${snapshot.error}'));
        } else if (!snapshot.hasData || snapshot.data!.isEmpty) {
        return Center(child: Text('No data found'));
        } else {
      return ListView.builder(
          itemCount: snapshot.data!.length,
          itemBuilder: (context, index) {
            var item = snapshot.data![index];
            return Card(
              child: Column(
                children: [
                  ListTile(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => MyTest(),
                            settings: RouteSettings(
                                arguments: item["id"]
                            )
                        ),
                      );
                    },
                    title: Text(item['name']),
                    subtitle: Text(item['email']),
                  )

                ],
              ),
            );
          });
    }
        },
      ),
    );
  }
}
