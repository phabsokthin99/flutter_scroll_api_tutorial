import 'package:flutter/material.dart';

class MyTest extends StatefulWidget {
  const MyTest({super.key});

  @override
  State<MyTest> createState() => _MyTestState();
}

class _MyTestState extends State<MyTest> {

  @override
  Widget build(BuildContext context) {
    // final id = ModalRoute.of(context)!.settings.arguments as int;
    // final id = ModalRoute.of(context)!.settings.arguments as String;
    final item = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("App Bar"),
          titleTextStyle: TextStyle(color: Colors.white, fontSize: 20),
          backgroundColor: Colors.blue,
        ),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              children: [
                SizedBox(height: 10,),
                Image.network(item['image'], width: 200, height: 200,),
                SizedBox(height: 20,),
                Text(item['title'], style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                SizedBox(height: 20,),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text('Price: \$${item['price']}'.toString(), style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                  ],
                ),
                SizedBox(height: 10,),
                Text('${item['description']}'),
                SizedBox(height: 15),
                ElevatedButton(onPressed: (){}, child: Text("Add To Cart",),style: ElevatedButton.styleFrom(

                  backgroundColor: Colors.blue,
                  onPrimary: Colors.white

                ),)
              ],
            ),
          )
        ),
      ),
    );
  }
}
