import 'package:flutter/material.dart';

class CatalogModel{
  static final  items = [
    Item(
      id: "code123",
      name: "Iphone 12 pro max",
      desc: "Apple Iphone made by Google",
      price: 999,
      color: "orange",
      image: "https://i5.walmartimages.com/asr/7497fd45-fece-4cc5-9c33-05ab195c985f.87a85446b3ee239f0495dc82a87788cf.jpeg",
    ),
    

    // Add more items here
  ];
}

class Item {
  final String id;
  final String name;
  final String desc;
  final num price;
  final String color;
  final String image;

  Item({
    required this.id,
    required this.name,
    required this.desc,
    required this.price,
    required this.color,
    required this.image,
  });
}

