
import 'package:array/Models/catalog.dart';
import 'package:array/Widgets/item_Widget.dart';
import 'package:flutter/material.dart';

class MyListViewBuilder extends StatefulWidget {
  const MyListViewBuilder({super.key});

  @override
  State<MyListViewBuilder> createState() => _MyListViewBuilderState();
}

class _MyListViewBuilderState extends State<MyListViewBuilder> {

  @override
  Widget build(BuildContext context) {

    // final dummyList = List.generate(10, (index) => CatalogModel.items[0]);

    return Scaffold(
      appBar: AppBar(
        title: Text("List Bars"),
        backgroundColor: Colors.blue,
        titleTextStyle: TextStyle(color: Colors.white),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView.builder(
          itemCount: CatalogModel.items.length,
          //   itemCount: dummyList.length,
          itemBuilder: (context, index){
            return ItemWidget(
                item: CatalogModel.items[index]
                // item: dummyList[index]
            );
          }
        ),
      ),
    );
  }
}
