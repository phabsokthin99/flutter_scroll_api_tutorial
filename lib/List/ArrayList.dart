import 'package:flutter/material.dart';

class ArrayList extends StatefulWidget {
  const ArrayList({super.key});

  @override
  State<ArrayList> createState() => _ArrayListState();
}

class _ArrayListState extends State<ArrayList> {

  final List<String> items= [
    'items',
    "how are you",
    "ilove you",
    "Hello boys"
  ];

  final List<String> items1 = [
    "SokthinView",
    "How are you boy",
    "Whate is your name"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Menu"),
        backgroundColor: Colors.orange,
        titleTextStyle: TextStyle(color: Colors.white),
      ),
      body: Column(
        children: [
          Container(
            child: Column(
              children: items.map((e) => Text(e)).toList(),
            ),
          ),
          SizedBox(height: 20,),
          Container(
            child: Column(
              children: items1.map((e){
               return Text(e) ;
              }).toList(),
            ),
          )
        ]
      ),
    );
  }
}
