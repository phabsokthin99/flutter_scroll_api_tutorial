import 'package:array/Api/ApiList.dart';
import 'package:array/Api/ApiTutorial.dart';
import 'package:array/List/ArrayList.dart';
import 'package:array/List/ListBuilder.dart';
import 'package:array/List/ListViewBuilder.dart';
import 'package:array/Scroll/ApiScroll.dart';
import 'package:array/Scroll/Scroll.dart';
import 'package:array/Scroll/TestScroll.dart';
import 'package:array/Test/List.dart';
import 'package:array/buttonNavigation/buttonNavigation.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}


class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      // home: ArrayList(),
      // home: MyListViewBuilder(),
      // home: ListBuilder(),
      // home: MyList(),
      // home: MyApi(),
      // home: MyAppList(),
      // home: MyScroll(),
      // home: MyTestScroll(),
      // home: MyApiScroll(),
      home: MyButtonNavigation(),
    );
  }
}
